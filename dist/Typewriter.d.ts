declare type CallableFuncPromise = () => Promise<void>;
interface Options {
    speed: number;
    cursorSpeed: number;
    cursorColor: string;
    endDot: boolean;
}
interface QueueTask {
    promise: CallableFuncPromise;
    resolve: CallableFunction;
    reject: CallableFunction;
}
declare class TypewriterQueue {
    static queue: Array<QueueTask>;
    static stop: boolean;
    static workingOnPromise: boolean;
    static add(promise: CallableFuncPromise): Promise<{}>;
    static dequeue(): false | undefined;
}
declare class Typewriter {
    private appElement;
    private cursorElement;
    private speed;
    private cursorSpeed;
    private state;
    constructor(elemId: string, options: Options);
    private showCursor;
    private deleteLast;
    private typeOne;
    wait(timeMs: number): Promise<void>;
    delete(numOfChars: number): Promise<void>;
    type(message: string): Promise<void>;
}
