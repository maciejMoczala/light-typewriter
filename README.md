# how it differs from other typewriter-ish packages?

well, it gives you much more freedom than most popular packages, you can delete parts of text, make the typer stop for a while, this gives it a "real human effect"

# how this works?

like all code... it's magic

but serioulsly:

```javascript
import { Typewriter } from "light-typewriter";

const app = new Typewriter("typewriter-element-id", {
  speed: 200, // typing speed
  cursorSpeed: 500, // blinking speed
  cursorColor: "aquamarine", // as name suggests
  endDot: true, // show dot after cursor
});

app.queue.add(
  [
    app.wait.bind(app, 700),
    // Let me help you
    app.type.bind(app, "Let me help you"),
    app.wait.bind(app, 2000),
    // Let me
    app.delete.bind(app, 8),
    // Let me type for you
    app.type.bind(app, "type for you"),
    app.wait.bind(app, 2000),
  ],
  true // provide true for infinite loop
);
```

![showcase](https://moczala.com/cloud/showcase.gif)
you get the hang of it

# why I created this?

other typewriter-ish packages were either large in size, required jquery or didn't meet my requirements
