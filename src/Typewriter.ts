type CallableFuncPromise = () => Promise<void>;

interface Options {
  speed: number;
  cursorSpeed: number;
  cursorColor: string;
  endDot: boolean;
}
interface QueueTask {
  promise: CallableFuncPromise;
  resolve: CallableFunction;
  reject: CallableFunction;
}

class TypewriterQueue {
  private parentObject: Typewriter;
  private queue: Array<QueueTask> = [];
  private stop: boolean;
  private workingOnPromise: boolean;
  constructor(parentTypewriter: Typewriter) {
    this.parentObject = parentTypewriter;
    this.workingOnPromise = false;
    this.stop = false;
  }

  add(tasks: Array<CallableFuncPromise>, loop: boolean) {
    tasks.forEach((task, index) => {
      this.enlistPromiseToQueue(() => task());
      if (index === tasks.length - 1 && loop) {
        this.enlistPromiseToQueue;
      }
      index === tasks.length - 1 && loop
        ? this.enlistPromiseToQueue(() =>
            this.parentObject.delete().then(() => this.add(tasks, loop))
          )
        : null;
    });
  }

  private enlistPromiseToQueue(promise: CallableFuncPromise) {
    return new Promise(
      (resolve: CallableFunction, reject: CallableFunction) => {
        this.queue.push({
          promise,
          resolve,
          reject,
        });
        this.remove();
      }
    );
  }

  private remove() {
    if (this.workingOnPromise) {
      return false;
    }
    if (this.stop) {
      this.queue = [];
      this.stop = false;
      return;
    }
    const item = this.queue.shift();
    if (!item) {
      return false;
    }

    this.workingOnPromise = true;
    item.promise().then(() => {
      this.workingOnPromise = false;
      item.resolve();
      this.remove();
    });
  }
}

export class Typewriter {
  public queue: TypewriterQueue;
  private appElement: HTMLElement;
  private cursorElement: HTMLElement;
  private speed: number;
  private cursorSpeed: number;
  private state = {
    typing: false,
  };

  constructor(elemId: string, options: Options) {
    //this.appElement = document.getElementById(elemId);
    let wrapper = document.getElementById(elemId);
    if (!wrapper) {
      throw new Error(`Element with id: ${elemId} does not exist on document`);
    } else {
      wrapper.innerHTML = `<typewriter id='typew-app'></typewriter><cursor id='typew-cursor'></cursor>${
        options.endDot ? "<dot style='margin-left: 0.3em'>.</dot>" : ""
      }`;

      this.appElement = document.getElementById("typew-app")!;
      if (!this.appElement) {
        throw new Error(
          `Element with id: ${elemId} does not exist on document`
        );
      }
      this.cursorElement = document.getElementById("typew-cursor")!;
      this.cursorElement.style.fontSize = "1.3em";
      this.cursorElement.style.position = "absolute";
      this.cursorElement.style.lineHeight = ".6em";
      this.cursorElement.style.fontWeight = "300";
      this.cursorElement.style.color = options.cursorColor;
      this.speed = 1000;
      this.speed = options.speed;
      this.cursorSpeed = options.cursorSpeed;

      this.showCursor();

      this.queue = new TypewriterQueue(this);
    }
  }

  private showCursor() {
    const hideCursor = () => {
      setTimeout(() => {
        this.cursorElement.innerHTML = "";
        this.showCursor();
      }, this.cursorSpeed);
    };

    setTimeout(() => {
      this.cursorElement.innerHTML = "|";
      if (!this.state.typing) {
        hideCursor();
      } else {
        this.showCursor();
      }
    }, this.cursorSpeed);
  }

  private deleteLast() {
    return new Promise<void>((resolve: CallableFunction) => {
      setTimeout(() => {
        let currText = this.appElement.innerHTML;
        let result = currText.slice(0, currText.length - 1);
        this.appElement.innerHTML = result;
        resolve();
      }, this.speed / 2);
    });
  }

  private typeOne(char: string) {
    return new Promise<void>((resolve: CallableFunction) => {
      setTimeout(() => {
        this.appElement.innerHTML += char;
        resolve();
      }, this.speed);
    });
  }

  async wait(timeMs: number) {
    return new Promise<void>((resolve: CallableFunction) => {
      setTimeout(() => {
        resolve();
      }, timeMs);
    });
  }

  async delete(numOfChars?: number) {
    this.state.typing = true;
    return new Promise<void>(async (resolve: CallableFunction) => {
      numOfChars ? null : (numOfChars = this.appElement.innerHTML.length);
      await this.deleteLast().then(async () => {
        if (numOfChars! - 1 > 0) {
          await this.delete(numOfChars! - 1).then(() => resolve());
        } else {
          this.state.typing = false;
          resolve();
        }
      });
    });
  }

  async type(message: string) {
    this.state.typing = true;
    return new Promise<void>(async (resolve: CallableFunction) => {
      await this.typeOne(message[0]).then(async () => {
        if (message.length > 0) {
          message = message.slice(1, message.length);

          if (message.length === 0) {
            this.state.typing = false;
            resolve();
          } else {
            await this.type(message).then(() => resolve());
          }
        }
      });
    });
  }
}
